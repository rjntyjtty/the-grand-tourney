﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour
{
	//float Cooldown;
	public float Speed;
	public float Jump;
	Rigidbody2D Player;
	public Transform Ground_Check;
	//public Transform Launch_Point;
	//public GameObject Ability;

	public string Horizontal_Control, Vertical_Control;//, Ability_Control;

	void Start()
	{
		Player = GetComponent<Rigidbody2D>();
		//Cooldown = Time.time;
	}

	void Update()
	{

		if (Input.GetAxisRaw(Horizontal_Control) == 1)
		{
			Player.velocity = new Vector2(Speed, Player.velocity.y);
			transform.localScale = new Vector2(1, transform.localScale.y);
		}
		else
		if (Input.GetAxisRaw(Horizontal_Control) == -1)
		{
			Player.velocity = new Vector2(-Speed, Player.velocity.y);
			transform.localScale = new Vector2(-1, transform.localScale.y);
		}
		else
			Player.velocity = new Vector2(0, Player.velocity.y);

		Debug.DrawLine(this.transform.position, Ground_Check.position, Color.red);//drawline

		if (Physics2D.Linecast(transform.position, Ground_Check.position, 1 << LayerMask.NameToLayer("Ground")) != false && Input.GetAxisRaw(Vertical_Control) == 1)
			Player.velocity = new Vector2(Player.velocity.x, Jump);

		if(Player.position.y <= -5)
		{
			Player.position = new Vector2(0,0);
		}

	}
	/*
	private void FixedUpdate()
	{
		if (Input.GetAxisRaw(Ability_Control) == 1 && Cooldown <= Time.time)
		{
			Cooldown = Time.time + 0.1f;
			GameObject NinjastarClone = (GameObject)Instantiate(Ability, Launch_Point.position, Launch_Point.rotation);
			NinjastarClone.transform.localScale = transform.localScale;
		}


	}
	*/

}

