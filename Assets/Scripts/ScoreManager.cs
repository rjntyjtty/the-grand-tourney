﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour {

    public int p1_Score, p2_Score, MaxScore;
	public Text p1, p2;
    public GameObject RightPlayerWins, LeftPlayerWins;
    public bool GameFinished;

	void Start()
	{
        GameFinished = false;
        p1 = p1.GetComponent<Text>();
        p2 = p2.GetComponent<Text>();
        p1_Score = 0;
        p2_Score = 0;

		Time.timeScale = 1;

	}

    void Update()
    {
        p1.text = "" + p1_Score;
        p2.text = "" + p2_Score;

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("Main Menu");
        }

    }

	public void Addpoint(Side s)
	{
		switch (s)
		{
			case Side.left:
                p2_Score ++;

                if (p2_Score >= MaxScore && GameFinished == false)
                {
                    RightPlayerWins.SetActive(true);
                    GameFinished = true;
                }

				break;

			case Side.right:
                p1_Score ++;

                if (p1_Score >= MaxScore && GameFinished == false)
                {
                    LeftPlayerWins.SetActive(true);
                    GameFinished = true;
                }

                break;
		}

	}




}
