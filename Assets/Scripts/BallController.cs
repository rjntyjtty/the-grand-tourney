﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum Side
{
	left, right
}

public class BallController : MonoBehaviour {

	private Rigidbody2D Rb;
	ScoreManager Score;	
	public ParticleSystem RExplosion, LExplosion;
	public new Camera camera;
	public GameObject Player1, Player2;
	private Vector2 p1pos, p2pos;
	public bool Scored;
	private GameObject ChildOfRight, ChildOfLeft;	

	void Start () {
		Rb = GetComponent<Rigidbody2D>();
		Score = FindObjectOfType<ScoreManager>();
		Scored = false;

		for (int i = 0; i < Player1.transform.childCount; i++)
		{
			if (Player1.transform.GetChild(i).gameObject.activeSelf == true)
			{
				ChildOfLeft = Player1.transform.GetChild(i).gameObject;
			}
		}

		for (int i = 0; i < Player2.transform.childCount; i++)
		{
			if (Player2.transform.GetChild(i).gameObject.activeSelf == true)
			{
				ChildOfRight = Player2.transform.GetChild(i).gameObject;
			}
		}

		p1pos = ChildOfLeft.transform.position;
		p2pos = ChildOfRight.transform.position;

	}

	private void OnTriggerEnter2D(Collider2D other)
	{

		if (other.tag == "Goal" && Scored == false )
		{
			if (Rb.position.x < 0)
			{
				print("left");
				Rb.AddForce(Vector2.right*100,ForceMode2D.Impulse);
				Score.Addpoint(Side.left);
				LExplosion.Play();
				
			}
			else
			{
				print("right");
				Rb.AddForce(Vector2.left * 100, ForceMode2D.Impulse);
				Score.Addpoint(Side.right);
				RExplosion.Play();
			}

			StartCoroutine(Scoring());

		}//end of if its a goal
		 
	}//end of tigger

	IEnumerator Scoring()
	{
		Scored = true;
		Time.timeScale = 0.2f;
		float timetowait = Time.unscaledTime;
		while (Time.unscaledTime <= timetowait+3)
		{
			camera.transform.position = Vector2.MoveTowards(camera.transform.position, transform.position, 0.2f);
			yield return null; 
		}
		Time.timeScale = 1f;
		camera.transform.position = new Vector3(0, 10, 0);

		//yield return new WaitForSeconds(3f);
		Rb.transform.position = new Vector3(0, 10, 0);
		Rb.velocity = Vector2.zero;
		//ChildOfLeft.transform.position = p1pos;
		//ChildOfRight.transform.position = p2pos;

		for (int i = 0; i < Player1.transform.childCount; i++)
		{
			Player1.transform.GetChild(i).gameObject.transform.position = p1pos;
			Player1.transform.GetChild(i).gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			Player2.transform.GetChild(i).gameObject.transform.position = p2pos;
			Player2.transform.GetChild(i).gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		}


			Scored = false;
	}


}