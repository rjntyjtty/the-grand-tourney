﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

    public string SoccerMode, BasketMode;
    public Text NumberGoals;
    public int MaxGoals;

    public void StartSoccer()
    {
        SceneManager.LoadScene(SoccerMode);
    }

    public void StartBasket()
    {
        SceneManager.LoadScene(BasketMode);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void InputGoals()
    {
        MaxGoals = int.Parse(NumberGoals.text);
    }

}
