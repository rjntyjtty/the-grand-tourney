﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeSelector : MonoBehaviour {

	private int Child;
	public string Vertical_Control;
	public bool PressedDown;
	private float FireRate;
	
	void Start () {
		Child = 0;
		FireRate = Time.time;
		PressedDown = false;
	}
	

	void FixedUpdate()
	{
		if (Input.GetAxisRaw(Vertical_Control) == -1 && PressedDown == false)
		{

			PressedDown = true;


			Child++;

			if (Child >= transform.childCount)//disables current character
			{
				Child = 0;
				transform.GetChild(transform.childCount - 1).gameObject.SetActive(false);
				transform.GetChild(Child).gameObject.transform.position = transform.GetChild(transform.childCount - 1).gameObject.transform.position;
				transform.GetChild(Child).gameObject.transform.localScale = transform.GetChild(transform.childCount - 1).gameObject.transform.localScale;
			}
			else
			{
				transform.GetChild(Child - 1).gameObject.SetActive(false);
				transform.GetChild(Child).gameObject.transform.position = transform.GetChild(Child - 1).gameObject.transform.position;
				transform.GetChild(Child).gameObject.transform.localScale = transform.GetChild(Child - 1).gameObject.transform.localScale;
			}

			transform.GetChild(Child).gameObject.SetActive(true);
			
			
		}

		if (Input.GetAxisRaw(Vertical_Control) == 0)
			PressedDown = false;

	}




}
