﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceCube : MonoBehaviour
{
	public float Speed;
	private Rigidbody2D Rb;
	public float Duration;
	public bool Collided;
	float StayTime;
	private Collision2D CollidedObject;

	void Start()
	{
		Rb = GetComponent<Rigidbody2D>();
		Collided = false;
		StayTime = Time.time + Duration;
		
	}


	void FixedUpdate()
	{

		if (Collided == false)
			Rb.velocity = new Vector2(Speed * transform.localScale.x, Rb.velocity.y);
		else if (Rb.position != CollidedObject.rigidbody.position)
		{
			GetComponent<FixedJoint2D>().enabled = false;
			Rb.position = CollidedObject.rigidbody.position;
			GetComponent<FixedJoint2D>().enabled = true;
			GetComponent<FixedJoint2D>().connectedBody = CollidedObject.rigidbody;
		}

		if (Time.time >= StayTime)
		{
			if (Collided == true && CollidedObject.gameObject.layer == 10 && CollidedObject.gameObject.name != "Ball")
			CollidedObject.gameObject.GetComponentInParent<SlimeSelector>().enabled = true;

			Destroy(gameObject);
		}

	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.layer == 10 && Collided == false)
		{
			Collided = true;

			if (other.gameObject.name != "Ball")
			{
				other.gameObject.GetComponentInParent<SlimeSelector>().enabled = false;
			}

			StartCoroutine(Iceman(other));
		}

	}


	IEnumerator Iceman(Collision2D other)
	{
			CollidedObject = other;

			Rb.position = CollidedObject.gameObject.GetComponent<Rigidbody2D>().position;

			Rb.velocity = Vector2.zero;
			gameObject.transform.localScale = new Vector2(6, 5);

			other.gameObject.GetComponent<Rigidbody2D>().position = Rb.position;

			GetComponent<FixedJoint2D>().enabled = true;
			GetComponent<FixedJoint2D>().connectedBody = other.gameObject.GetComponent<Rigidbody2D>();
		yield return null;
	}




}
