﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class IceCubeAbility : MonoBehaviour
{

	private SpriteRenderer Sprite;
	private Color Basecolour;


	float Waittime;
	public Transform Launch_Point;
	public GameObject Ability;
	public float Cooldown;

	public string Ability_Control;

	void Start()
	{
		Waittime = Time.time;
		Sprite = GetComponent<SpriteRenderer>();
		Basecolour = Sprite.color;
	}

	private void FixedUpdate()
	{

		if (Waittime == Time.time)
			StartCoroutine(Flash());



		if (Input.GetAxisRaw(Ability_Control) == 1 && Waittime <= Time.time)
		{
			Waittime = Time.time + Cooldown;
			GameObject IceCubeClone = (GameObject)Instantiate(Ability, Launch_Point.position, Launch_Point.rotation);
			IceCubeClone.transform.localScale = transform.localScale;
		}
	}

	IEnumerator Flash()
	{
		Sprite.color = Color.yellow;
		yield return new WaitForSeconds(0.05f);
		Sprite.color = Basecolour;
	}


}