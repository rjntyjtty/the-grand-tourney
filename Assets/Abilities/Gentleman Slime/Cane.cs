﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cane : MonoBehaviour {

	public float slamForce;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.layer == 10)
		{
			if(other.gameObject.name == "Ball")
				other.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * slamForce, ForceMode2D.Impulse);
			else
			other.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 100 * slamForce, ForceMode2D.Impulse);
		}
	}

}
