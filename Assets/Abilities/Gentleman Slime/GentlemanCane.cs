﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GentlemanCane : MonoBehaviour
{

	private SpriteRenderer Sprite;
	private Color Basecolour;

	float FireRate;
	public Transform Launch_Point;
	public GameObject Ability;
	public float Cooldown;
	public bool Activated = false;
	public string Ability_Control;

	private void Start()
	{
		Sprite = GetComponent<SpriteRenderer>();
		Basecolour = Sprite.color;
	}

	private void FixedUpdate()
	{

		if (FireRate == Time.time)
			StartCoroutine(Flash());

		if (Input.GetAxisRaw(Ability_Control) == 1 && FireRate <= Time.time)
		{
			FireRate = Time.time + Cooldown;
			Activated = true;
			StartCoroutine(CaneSmash());

		}
	}

	IEnumerator CaneSmash()
	{
		GetComponentInChildren<Animator>().Play("GentlemanCaneSmash");
		yield return new WaitForSeconds(2f);
		Activated = false;
	}

	IEnumerator Flash()
	{
		Sprite.color = Color.yellow;
		yield return new WaitForSeconds(0.05f);
		Sprite.color = Basecolour;
	}

}