﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Dash : MonoBehaviour
{

	private float Waittime;
	public float Cooldown;

	public string Ability_Control;
	public float DashTime;
	public float DashForce;

	private SpriteRenderer Sprite;
	private Color Basecolour;
	private Rigidbody2D SlimeRB;
	private PlayerMove MovementControlls;

	public bool Activated;

	void Start()
	{
		Waittime = Time.time;
		Sprite = GetComponent<SpriteRenderer>();
		Basecolour = Sprite.color;
		SlimeRB = GetComponent<Rigidbody2D>();
		Activated = false;
		MovementControlls = GetComponent<PlayerMove>();

	}

	private void OnEnable()
	{
		Sprite.color = Basecolour;
		SlimeRB.velocity = new Vector2(SlimeRB.velocity.x, SlimeRB.velocity.y);
	}

	private void FixedUpdate()
	{

		if (Waittime == Time.time)
			StartCoroutine(Flash());

		if (Input.GetAxisRaw(Ability_Control) == 1 && Waittime <= Time.time)
		{
			Waittime = Time.time + Cooldown;
			StartCoroutine(SuperDash());
		}

		if(Activated == true)
		{
			SlimeRB.velocity = new Vector2(SlimeRB.velocity.x*DashForce, SlimeRB.velocity.y);
		}


	}


	IEnumerator Flash()
	{
		Sprite.color = Color.yellow;
		yield return new WaitForSeconds(0.05f);
		Sprite.color = Basecolour;
	}

	IEnumerator SuperDash()
	{
		Activated = true;
		Sprite.color = Color.green;
		yield return new WaitForSeconds(DashTime);
		Activated = false;
		Sprite.color = Basecolour;
	}


}