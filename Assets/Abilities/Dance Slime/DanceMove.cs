﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DanceMove : MonoBehaviour
{

	float FireRate;
	public Transform Launch_Point, top, right, left;
	public GameObject Ability;
	public float Cooldown, Jump;
	public bool Activated = false;
	public string Ability_Control;
	public string Vertical_Control;

	private void LateUpdate()
	{

		if ((Physics2D.Linecast(transform.position, top.position, 1 << LayerMask.NameToLayer("Ground")) != false || Physics2D.Linecast(transform.position, right.position, 1 << LayerMask.NameToLayer("Ground")) != false || Physics2D.Linecast(transform.position, left.position, 1 << LayerMask.NameToLayer("Ground")) != false) && Input.GetAxisRaw(Vertical_Control) == 1)
			GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, Jump);

		if (Physics2D.Linecast(transform.position, top.position, 1 << LayerMask.NameToLayer("Ground")) != false && Input.GetAxisRaw(Vertical_Control) == -1)
			GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, -Jump);

		if (Input.GetAxisRaw(Ability_Control) == 1 && FireRate <= Time.time)
		{
			FireRate = Time.time + Cooldown;
			Activated = true;
			StartCoroutine(DanceDanceRevolution());

		}
	}

	IEnumerator DanceDanceRevolution()
	{

		transform.Rotate(0,0,90);
		GetComponent<Rigidbody2D>().transform.localScale = new Vector2(2, 1);
		yield return new WaitForSeconds(2f);
		transform.Rotate(0, 0, 720);
		GetComponent<Rigidbody2D>().transform.localScale = new Vector2(1, 1);

		Activated = false;
	}

}