﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class NinjaStarAbility : MonoBehaviour
{

	private SpriteRenderer Sprite;
	private Color Basecolour;

	float FireRate;
	public Transform Launch_Point;
	public GameObject Ability;
	public float cooldown;

	public string Ability_Control;

	void Start()
	{
		FireRate = Time.time;
		Sprite = GetComponent<SpriteRenderer>();
		Basecolour = Sprite.color;
	}

	private void FixedUpdate()
	{

		if (FireRate == Time.time)
			StartCoroutine(Flash());

		if (Input.GetAxisRaw(Ability_Control) == 1 && FireRate <= Time.time)
		{

				FireRate = Time.time + cooldown;

			StartCoroutine(NinjaFury());

		}
	}

	IEnumerator NinjaFury()
	{
		int StarCounter = 0;
		while (StarCounter <= 10)
		{
			GameObject NinjastarClone = (GameObject)Instantiate(Ability, Launch_Point.position, Launch_Point.rotation);
			NinjastarClone.transform.localScale = transform.localScale;
			StarCounter++;
			yield return new WaitForSeconds(0.1f);
		}

		yield return null;
	}

	IEnumerator Flash()
	{
		Sprite.color = Color.yellow;
		yield return new WaitForSeconds(0.05f);
		Sprite.color = Basecolour;
	}

}