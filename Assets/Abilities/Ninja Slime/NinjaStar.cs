﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaStar : MonoBehaviour {

    public float Speed;
    private Rigidbody2D Rb;

	void Start () {
        Rb = GetComponent<Rigidbody2D>();
	}
	

	void FixedUpdate () {
        Rb.velocity = new Vector2(Speed*transform.localScale.x,Rb.velocity.y);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
		if(other.gameObject.layer==9)
        Destroy(gameObject);
    }

}
